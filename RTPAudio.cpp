#include "RTPAudio.h"
#include "RtAudio.h"
#include <fstream> // file saving

using namespace RTPAudio;

long long unsigned int recordCallbackTimeSum;
long long unsigned int recordCallbackInvokedTimes;

// Private functions

// Remap from RtAudio types to my types and vice versa
RTPAudio::RTPError::Type  RtAudioErr2RTPErr(RtAudioError &e);
RTPAudio::Api RtAudioApi2RTPAudioApi(RtAudio::Api);
RtAudio::Api  RTPAudioApi2RtAudioApi(RTPAudio::Api api);

void checkError(int error);

void printOpusError(int error);


/* Callback for recording */
int input(void                */*outputBuffer*/,
          void                *inputBuffer,
          unsigned int        /*nBufferFrames*/,
          double              /*streamTime*/,
          RtAudioStreamStatus /*status*/,
          void                *data);


void RTPServer::initAndListen(uint16_t portBase)
{
    jrtplib::RTPSessionParams sessionParams;
    // How many samples per second
    sessionParams.SetOwnTimestampUnit(1.0 / 44100.0);
    sessionParams.SetAcceptOwnPackets(true);

    jrtplib::RTPUDPv4TransmissionParams transmissionParams;;
    transmissionParams.SetPortbase(portBase);

    int status = m_session.Create(sessionParams,&transmissionParams);

    if (status < 0){
        throw RTPError(jrtplib::RTPGetErrorString(status), RTPError::NETWORK_ERROR);
    }
}

void MyRTPSession::printClients()
{
    for (auto const& it  : m_SSRCtoClient){
        printf("SSRC: %x samplerate: %d channels: %d samplesize: %d\n",
               it.first,
               it.second->sampleRate,
               it.second->channels,
               it.second->sampleSize);
    }
}

void MyRTPSession::OnBYEPacket(jrtplib::RTPSourceData *dat)
{
    using namespace jrtplib;
    if (dat->IsOwnSSRC()){
        return;
    }

    uint32_t ip;
    uint16_t port;

    if (dat->GetRTPDataAddress() != nullptr){
        const auto *addr = (const RTPIPv4Address *)(dat->GetRTPDataAddress());
        ip = addr->GetIP();
        port = addr->GetPort();
    }
    else if (dat->GetRTCPDataAddress() != nullptr){
        const auto *addr = (const RTPIPv4Address *)(dat->GetRTCPDataAddress());
        ip = addr->GetIP();
        port = addr->GetPort()-1;
    }
    else{
        return;
    }

    RTPIPv4Address dest(ip,port);
    DeleteDestination(dest);


    struct in_addr inaddr;
    inaddr.s_addr = htonl(ip);
    std::cout << "Deleting destination " << std::string(inet_ntoa(inaddr)) << ":" << port << std::endl;

    size_t len = 0;
    char byeReason[1024];
    memset(byeReason, 0, sizeof(byeReason));
    auto reason_p = dat->GetBYEReason(&len);
    if (len != 0){
        if (len >= sizeof(byeReason)){
            len = sizeof(byeReason) - 1;
        }
        memcpy(byeReason, reason_p, len);
        printf("Reason: %s\n", byeReason);
    }

    auto SSRC = dat->GetSSRC();
    delete m_SSRCtoClient[SSRC];
    m_SSRCtoClient[SSRC] = nullptr;
}

void MyRTPSession::OnNewSource(jrtplib::RTPSourceData *dat)
{
    using namespace jrtplib;

    if (dat->IsOwnSSRC()){
        return;
    }

    uint32_t ip;
    uint16_t port;

    if (dat->GetRTPDataAddress() != nullptr){
        const auto *addr = (const RTPIPv4Address *)(dat->GetRTPDataAddress());
        ip = addr->GetIP();
        port = addr->GetPort();
    }
    else if (dat->GetRTCPDataAddress() != nullptr){
        const auto *addr = (const RTPIPv4Address *)(dat->GetRTCPDataAddress());
        ip = addr->GetIP();
        port = addr->GetPort() - 1;
    }
    else
        return;

    RTPIPv4Address dest(ip,port);
    AddDestination(dest);

    struct in_addr inaddr;
    inaddr.s_addr = htonl(ip);
    std::cout << "Adding destination " << std::string(inet_ntoa(inaddr)) << ":" << port << std::endl;
}

void MyRTPSession::OnRemoveSource(jrtplib::RTPSourceData *dat)
{
    using namespace jrtplib;

    if (dat->IsOwnSSRC()){
        return;
    }

    if (dat->ReceivedBYE()){
        return;
    }

    uint32_t ip;
    uint16_t port;

    if (dat->GetRTPDataAddress() != nullptr){
        const auto *addr = (const RTPIPv4Address *)(dat->GetRTPDataAddress());
        ip = addr->GetIP();
        port = addr->GetPort();
    }
    else if (dat->GetRTCPDataAddress() != nullptr){
        const auto *addr = (const RTPIPv4Address *)(dat->GetRTCPDataAddress());
        ip = addr->GetIP();
        port = addr->GetPort() - 1;
    }
    else{
        return;
    }

    RTPIPv4Address dest(ip,port);
    DeleteDestination(dest);

    struct in_addr inaddr;
    inaddr.s_addr = htonl(ip);
    std::cout << "Deleting destination " << std::string(inet_ntoa(inaddr)) << ":" << port << std::endl;

    auto SSRC = dat->GetSSRC();
    delete m_SSRCtoClient[SSRC];
    m_SSRCtoClient[SSRC] = nullptr;
}

void MyRTPSession::OnRTCPSDESItem(jrtplib::RTPSourceData *dat,
                                  jrtplib::RTCPSDESPacket::ItemType t,
                                  const void *itemData,
                                  size_t itemLen)
{
    using namespace jrtplib;

    if (dat->IsOwnSSRC()){
        return;
    }

    char msg[1024];
    memset(msg, 0, sizeof(msg));

    if (itemLen >= sizeof(msg)){
        itemLen = sizeof(msg) - 1;
    }

    auto SSRC = (unsigned int)dat->GetSSRC();
    memcpy(msg, itemData, itemLen);

    switch(t){

        case RTCPSDESPacket::None:
            printf("SSRC %x: Received SDES None item: %s\n", SSRC, msg);
            break;

        case RTCPSDESPacket::CNAME:
            printf("SSRC %x: Received SDES CNAME item: %s\n", SSRC, msg);
            m_SSRCtoCNAME[SSRC] = std::string(msg);
            // Check if client is present unnecessary because
            // CNAME always arrives later than first packet from client
            if (m_SSRCtoClient[SSRC] == nullptr){
                // TODO: workaround for client because client doesnt store client info so here we do have a
                // TODO nullptr access, also this needs to be deleted somewhere
                m_SSRCtoClient[SSRC] = new clientInfo;
            }
            m_SSRCtoClient[SSRC]->CNAME = std::string(msg);
            break;

        case RTCPSDESPacket::NAME:
            printf("SSRC %x: Received SDES NAME item: %s\n", SSRC, msg);
            break;

        case RTCPSDESPacket::EMAIL:
            printf("SSRC %x: Received SDES EMAIL item: %s\n", SSRC, msg);
            break;

        case RTCPSDESPacket::PHONE:
            printf("SSRC %x: Received SDES PHONE item: %s\n", SSRC, msg);
            break;

        case RTCPSDESPacket::LOC:
            printf("SSRC %x: Received SDES LOC item: %s\n", SSRC, msg);
            break;

        case RTCPSDESPacket::TOOL:
            printf("SSRC %x: Received TOOL SDES item: %s\n", SSRC, msg);
            break;

        case RTCPSDESPacket::PRIV:
            printf("SSRC %x: Received SDES PRIV item: %s\n", SSRC, msg);
            break;

        case RTCPSDESPacket::Unknown:
            printf("SSRC %x: Received SDES Unknown item: %s\n", SSRC, msg);
            break;

        case RTCPSDESPacket::NOTE:
            printf("SSRC %x: Received SDES NOTE item: %s\n", SSRC, msg);
            break;
    }
}

void MyRTPSession::OnRTCPSenderReport(jrtplib::RTPSourceData *dat)
{
    using namespace jrtplib;

    if (dat->IsOwnSSRC()){
        return;
    }

    uint32_t ip;
    uint16_t port;

    if (dat->GetRTPDataAddress() != nullptr)
    {
        const auto *addr = (const RTPIPv4Address *)(dat->GetRTPDataAddress());
        ip = addr->GetIP();
        port = addr->GetPort();
    }
    else if (dat->GetRTCPDataAddress() != nullptr)
    {
        const auto *addr = (const RTPIPv4Address *)(dat->GetRTCPDataAddress());
        ip = addr->GetIP();
        port = addr->GetPort() - 1;
    }
    else{
        return;
    }


    struct in_addr inaddr;
    inaddr.s_addr = htonl(ip);

    uint32_t packetCount = dat->SR_GetPacketCount();
    uint32_t byteCount = dat->SR_GetByteCount();
    //dat->SR_GetNTPTimestamp();
    //dat->SR_GetRTPTimestamp();
    //dat->SR_HasInfo();

    printf("SSRC %x: Received Sender Report %s:%d Byte count:%d Packet count:%d\n",
           (unsigned int)dat->GetSSRC(),
           inet_ntoa(inaddr),
           port,
           byteCount,
           packetCount);
}

void MyRTPSession::OnRTCPReceiverReport(jrtplib::RTPSourceData *dat)
{
    using namespace jrtplib;
    if (dat->IsOwnSSRC()){
        return;
    }

    uint32_t ip;
    uint16_t port;

    if (dat->GetRTPDataAddress() != nullptr)
    {
        const auto *addr = (const RTPIPv4Address *)(dat->GetRTPDataAddress());
        ip = addr->GetIP();
        port = addr->GetPort();
    }
    else if (dat->GetRTCPDataAddress() != nullptr)
    {
        const auto *addr = (const RTPIPv4Address *)(dat->GetRTCPDataAddress());
        ip = addr->GetIP();
        port = addr->GetPort() - 1;
    }
    else{
        return;
    }
    struct in_addr inaddr;
    inaddr.s_addr = htonl(ip);

    double fractionLost = dat->RR_GetFractionLost();
    int32_t packetsLost = dat->RR_GetPacketsLost();
    uint32_t jitter = dat->RR_GetJitter();

    printf("SSRC %x: Received Receiver Report %s:%d Packets lost:%d Fraction lost:%.3f Jitter:%d\n",
           (unsigned int) dat->GetSSRC(),
           inet_ntoa(inaddr),
           port,
           packetsLost,
           fractionLost,
           jitter);

}

void MyRTPSession::OnPollThreadStep()
{
    using namespace jrtplib;

    BeginDataAccess();
    // check incoming packets
    if (GotoFirstSourceWithData()){
        do{
            RTPPacket *pack;
            RTPSourceData *srcdat;
            srcdat = GetCurrentSourceInfo();

            while ((pack = GetNextPacket()) != nullptr){
                ProcessRTPPacket(*srcdat,*pack);
                DeletePacket(pack);
            }
        } while (GotoNextSourceWithData());
    }
    EndDataAccess();
}

void MyRTPSession::ProcessRTPPacket(const jrtplib::RTPSourceData &dat, const jrtplib::RTPPacket &packet)
{
    auto SSRC = dat.GetSSRC();

    int frameSize = reinterpret_cast<uint32_t*>(packet.GetExtensionData())[3];
    // Done only once for given SSRC
    if (m_SSRCtoClient[SSRC] == nullptr){
        // Client ALWAYS sends transmission params in header extension - 16 bytes in total
        unsigned int sampleRate = reinterpret_cast<uint32_t*>(packet.GetExtensionData())[0];
        unsigned int channels = reinterpret_cast<uint32_t*>(packet.GetExtensionData())[1];
        unsigned int sampleSize = reinterpret_cast<uint32_t*>(packet.GetExtensionData())[2];
        m_SSRCtoClient[SSRC] = new clientInfo(sampleRate, channels, sampleSize, frameSize, SSRC);

        printf("SampleRate: %d\n"
               "channels: %d\n"
               "sampleSize: %d\n",
               m_SSRCtoClient[SSRC]->sampleRate,
               m_SSRCtoClient[SSRC]->channels,
               m_SSRCtoClient[SSRC]->sampleSize);
    }

    /*
    if (frameSize > previously allocated frameSize)
     buffer for decoded frames needs to be resized
     */


    // Receive samples and save to file
    auto opusPayloadLen = (opus_int32)packet.GetPayloadLength();
    uint8_t *payload = packet.GetPayloadData();

    int decodedFramesNum = opus_decode(m_SSRCtoClient[SSRC]->decoder,
                                       payload,
                                       opusPayloadLen,
                                       (m_SSRCtoClient[SSRC]->decoded),
                                       frameSize,
                                       0);

    // doesn't have to take sample size into account because
    // opus_int16 is always 2 bytes
    int framesToSave = decodedFramesNum * m_SSRCtoClient[SSRC]->channels;
    m_SSRCtoClient[SSRC]->wavFile.writeSamples(m_SSRCtoClient[SSRC]->decoded,
                                               m_SSRCtoClient[SSRC]->decoded + framesToSave);

}

RTPClient::RTPClient()
    : m_audio(nullptr),
      m_encodingTimesSum(0),
      m_noOfEncodings(0),
      m_encodedPacketSizeSum(0)
{
    m_bufferData.samples = nullptr;
    m_encoder = nullptr;
    // alloc  encoded buffer - max we can send and encode at once
    // 120ms, 48000Hz, 2 channels, sample size 2
    m_encoded = new unsigned char[23040];
}

RTPClient::~RTPClient() {
    if (m_audio->isStreamOpen()){
        m_audio->closeStream();
    }
    delete m_audio;
    // TODO: Think about closing session (maybe send bye packet?)
    delete m_bufferData.samples;

    if (m_encoder != nullptr) {
        opus_encoder_destroy(m_encoder);
    }

    printf("Average encoding time: %llu microseconds.\n",
           m_encodingTimesSum / m_noOfEncodings);
    printf("Average encoded packet size: %llu bytes.\n",
            m_encodedPacketSizeSum / m_noOfEncodings);
    printf("Recording callback invoked %llu times.\n",
            recordCallbackInvokedTimes);
    printf("Average circular buffer filling from record callback is %llu microseconds.\n",
            recordCallbackTimeSum / recordCallbackInvokedTimes);
    delete[] m_encoded;
}


RTPBufferData *RTPClient::setup(RTPRecordParams recordParams, RTPConnectionParams connParams) {

    RtAudio::Api api = RTPAudioApi2RtAudioApi(recordParams.api);
    m_audio = new RtAudio(api);
    recordParams.device = deviceValid(*m_audio, recordParams.device) ? recordParams.device : m_audio->getDefaultInputDevice();

    int (*recordCallBack)(void*, void*, unsigned int, double, RtAudioStreamStatus, void*) = input;

    RtAudio::StreamParameters inputParams;
    inputParams.deviceId = recordParams.device;
    inputParams.nChannels = recordParams.channels;
    inputParams.firstChannel = 0;    /* First channel index on device (default = 0). */

    try{
        m_audio->openStream(nullptr,
                            &inputParams,
                            recordParams.sampleType,
                            recordParams.sampleRate,
                            &recordParams.nBufferFrames,
                            recordCallBack,
                            (void*)&m_bufferData);
    }

    catch (RtAudioError& e){
        throw RTPError(e.getMessage(), RtAudioErr2RTPErr(e));
    }

    switch(recordParams.sampleType){

        case RTAUDIO_SINT8:
            m_bufferData.bufferBytes = recordParams.nBufferFrames * recordParams.channels * sizeof(int8_t);
            m_bufferData.sampleSize = sizeof(int8_t);
            break;

        case RTAUDIO_SINT16:
            m_bufferData.bufferBytes = recordParams.nBufferFrames * recordParams.channels* sizeof(int16_t);
            m_bufferData.sampleSize = sizeof(int16_t);
            break;

        case RTAUDIO_SINT32:
            m_bufferData.bufferBytes = recordParams.nBufferFrames * recordParams.channels * sizeof(int32_t);
            m_bufferData.sampleSize = sizeof(int32_t);
            break;

        case RTAUDIO_FLOAT32:
            m_bufferData.bufferBytes = recordParams.nBufferFrames * recordParams.channels * sizeof(float);
            m_bufferData.sampleSize = sizeof(float);
            break;

        case RTAUDIO_FLOAT64:
            m_bufferData.bufferBytes = recordParams.nBufferFrames * recordParams.channels * sizeof(double);
            m_bufferData.sampleSize = sizeof(double);
            break;

        default:
            throw RTPError("Bad sample format", RTPError::FORMAT_ERROR);
    }

    m_bufferData.channels = recordParams.channels;
    m_bufferData.sampleRate = m_audio->getStreamSampleRate();

    // Just in case - if is nullptr this has no effect
    delete  m_bufferData.samples;
    unsigned int minute = 60;

    // Alloc a minute of stream
    m_bufferData.samples = new bounded_buffer<int8_t>(minute *
                                                      m_bufferData.sampleSize *
                                                      m_bufferData.sampleRate *
                                                      m_bufferData.channels);

    using namespace jrtplib;

    RTPSessionParams sessionParams;
    // How many samples per second?
    sessionParams.SetOwnTimestampUnit(1.0 / double(m_audio->getStreamSampleRate()));
    sessionParams.SetAcceptOwnPackets(true);

    RTPUDPv4TransmissionParams transmissionParams;
    transmissionParams.SetPortbase(connParams.portBase);

    int status = m_session.Create(sessionParams, &transmissionParams);
    if (status < 0){
        throw RTPError(RTPGetErrorString(status), RTPError::NETWORK_ERROR);
    }

    if (connParams.localName.length() != 0) {
        m_session.SetLocalName(connParams.localName.c_str(), connParams.localName.length());
        m_session.SetNameInterval(1);
    }

    m_session.SetDefaultPayloadType(96); // 96 - Dynamic type
    m_session.SetDefaultMark(false);
    m_session.SetDefaultTimestampIncrement(400); // TODO: Change that

    uint32_t destIP = inet_addr(connParams.destIP.c_str());
    if (destIP == INADDR_NONE){
        throw RTPError("Bad IP address specified", RTPError::IP_ERROR);
    }
    destIP = ntohl(destIP);

    RTPIPv4Address addr(destIP, connParams.destPort);

    status = m_session.AddDestination(addr);
    if (status < 0){
        throw RTPError(RTPGetErrorString(status), RTPError::NETWORK_ERROR);
    }

    int error = OPUS_OK;
    m_encoder = opus_encoder_create(m_bufferData.sampleRate,
                                    m_bufferData.channels,
                                    OPUS_APPLICATION_AUDIO,
                                    &error);

    if (error != OPUS_OK) {
        printOpusError(error);
        return nullptr;
    }

    return &m_bufferData;
}

bool RTPClient::deviceValid(RtAudio &audio, unsigned int deviceNum)
{
    RtAudio::DeviceInfo info;
    try{
        info = audio.getDeviceInfo(deviceNum);
    }
    catch(RtAudioError& e){
        // TODO: throw exception?
        e.printMessage();
        return false;
    }
    /* If a device is busy or otherwise unavailable, the structure member probed will be false */
    return info.probed;
}

void RTPClient::getCompiledApis(std::vector<RTPAudio::Api> &apis)
{
    std::vector<RtAudio::Api> RtAudioAPIs;
    RtAudio::getCompiledApi(RtAudioAPIs);
    for (auto api : RtAudioAPIs){
        apis.push_back(RtAudioApi2RTPAudioApi(api));
    }
}

std::string RTPClient::getApiDisplayName(RTPAudio::Api api)
{
    RtAudio::Api RtAudioAPI = RTPAudioApi2RtAudioApi(api);
    return RtAudio::getApiDisplayName(RtAudioAPI);
}

unsigned int RTPClient::getDefaultInputDevice(RTPAudio::Api api)
{
    RtAudio::Api RtAudioAPI = RTPAudioApi2RtAudioApi(api);
    RtAudio audio(RtAudioAPI);
    return audio.getDefaultInputDevice();
}


unsigned int RTPClient::getDeviceCount(RTPAudio::Api api)
{
    RtAudio::Api RtAudioAPI = RTPAudioApi2RtAudioApi(api);
    RtAudio audio(RtAudioAPI);
    return (audio.getDeviceCount());
}

// TODO: should return struct with parameters
int RTPClient::displayDeviceInfo(RTPAudio::Api api, unsigned int deviceNum)
{
    RtAudio::Api RtAudioAPI = RTPAudioApi2RtAudioApi(api);
    RtAudio audio(RtAudioAPI);
    RtAudio::DeviceInfo info;
    try{
        info = audio.getDeviceInfo(deviceNum);
    }
    catch(RtAudioError& e){
        // TODO: throw exception?
        e.printMessage();
        return EXIT_FAILURE;
    }
    /* If a device is busy or otherwise unavailable, the structure member probed will be false */
    if (!info.probed){
        fprintf(stderr, "Device busy or unavailable.\n");
        return EXIT_FAILURE;
    }

    printf("Device %d(%s)\n", deviceNum, info.name.c_str());
    printf("   Output channels: %d\n",   info.outputChannels);
    printf("   Input channels: %d\n",    info.inputChannels);
    printf("   Duplex channels: %d\n",   info.duplexChannels);
    printf("   Default output: %s\n",  ((info.isDefaultOutput) ? "yes" : "no") );
    printf("   Default input: %s\n",   ((info.isDefaultInput) ? "yes" : "no") );
    printf("   Supported sample rates: ");

    for (const auto sampleRate : info.sampleRates){
        printf("%d ", sampleRate);
    }
    printf("\n");

    printf("Preferred sample rate: %d\n", info.preferredSampleRate);
    printf("Supported data formats:\n");
    if (info.nativeFormats & RTAUDIO_SINT8)   printf("      signed 8-bit integer\n");
    if (info.nativeFormats & RTAUDIO_SINT16)  printf("      signed 16-bit integer\n");
    if (info.nativeFormats & RTAUDIO_SINT24)  printf("      signed 24-bit integer\n");
    if (info.nativeFormats & RTAUDIO_SINT32)  printf("      signed 32-bit integer\n");
    if (info.nativeFormats & RTAUDIO_FLOAT32) printf("      32-bit float normalized between +/- 1.0\n");
    if (info.nativeFormats & RTAUDIO_FLOAT64) printf("      64-bit double normalized between +/- 1.0\n");
    printf("\n");
    return EXIT_SUCCESS;
}

void RTPClient::startRecording()
{
    try{
        m_audio->startStream();
    }
    catch(RtAudioError& e){
        throw RTPError(e.getMessage(), RtAudioErr2RTPErr(e));
    }
}


/* Packet len should be 2.5ms, 5ms, 10ms, 20ms, 40ms, 60ms, 80ms, 100ms or 120ms */
void RTPClient::SendPacket(const void *data, size_t len)
{
    uint32_t frameSize = len / m_bufferData.sampleSize / m_bufferData.channels;
    // TODO - We always send the same stream parameters,
    // TODO - so I could move that somewhere
    const uint16_t headerExtID = 1;
    const size_t numHeaderExtWords = 4;
    uint32_t streamParams[numHeaderExtWords] = {m_bufferData.sampleRate,
                                                m_bufferData.channels,
                                                m_bufferData.sampleSize,
                                                0};
    streamParams[3] = frameSize;
    //int status = m_session.SendPacketEx(data, len, headerExtID, streamParams, numHeaderExtWords);

    auto toEncode = (const opus_int16*) data;

    jrtplib::RTPTime start = jrtplib::RTPTime::CurrentTime();
    opus_int32 encodedLen = opus_encode(m_encoder,
                                        toEncode,
                                        frameSize,
                                        m_encoded,
                                        len);
    jrtplib::RTPTime stop = jrtplib::RTPTime::CurrentTime();
    stop -= start;
    m_encodedPacketSizeSum += encodedLen;
    m_noOfEncodings++;
    m_encodingTimesSum += stop.GetMicroSeconds();
    if ( encodedLen < 0){
        printOpusError(encodedLen);
    }
    // TODO: throw exception?
    int status = m_session.SendPacketEx(m_encoded,
                                        encodedLen,
                                        headerExtID,
                                        streamParams,
                                        numHeaderExtWords);
    checkError(status);
}

void RTPClient::sendBYE(const jrtplib::RTPTime &maxWaitTime, const std::string byeReason)
{
    m_session.BYEDestroy(maxWaitTime, byeReason.c_str(), byeReason.length());
}

// This function checks if there was a RTP error. If so, it displays an error
// message and exists.

void checkError(int error)
{
    if (error < 0)
    {
        std::cout << "ERROR: " << jrtplib::RTPGetErrorString(error) << std::endl;
        exit(-1);
    }
}

/* Callback for recording */
int input(void*               /*outputBuffer*/,
          void*               inputBuffer,
          unsigned int        /*nBufferFrames*/,
          double              /*streamTime*/,
          RtAudioStreamStatus /*status*/,
          void*               data)
{
    jrtplib::RTPTime start = jrtplib::RTPTime::CurrentTime();
    recordCallbackInvokedTimes++;
    auto *buffData = (RTPBufferData*) data;
    auto input_p = (int8_t*) inputBuffer;

    for (unsigned int i = 0; i < buffData->bufferBytes; i++){
        buffData->samples->push_front(input_p[i]);
    }
    jrtplib::RTPTime stop = jrtplib::RTPTime::CurrentTime();
    stop -= start;
    recordCallbackTimeSum += stop.GetMicroSeconds();
    return 0;
    // TODO: return different value when transmission is finished
}

int saveToWAV(const std::string &fileName,
              std::vector<opus_int16> &recordBuffer,
              unsigned int sampleSize,
              unsigned int sampleRate,
              unsigned int nChannels)
{
    /* Open the file and save the buffer */
    std::ofstream ofs(fileName, std::ios::binary);
    if (!ofs){
        std::cerr << "Error opening file" << fileName << "." << std::endl;
        return EXIT_FAILURE;
    }

    int BITS_PER_SAMPLE = sampleSize * CHAR_BIT;
    int LENGTH_OF_FORMAT_DATA = 16;
    int TYPE_OF_FORMAT = 1;   // PCM
    int BYTE_RATE = (sampleRate * BITS_PER_SAMPLE * nChannels) / CHAR_BIT;
    int DATA_BLOCK_SIZE = (BITS_PER_SAMPLE * nChannels) / CHAR_BIT; // size of two samples, one for each channel, in bytes

    // Write header - first 44 bytes.
    ofs.write("RIFF----WAVEfmt ", 16);  // file size to be filled later
    ofs.write(reinterpret_cast<char*>( &LENGTH_OF_FORMAT_DATA), 4); // 16 for PCM
    ofs.write(reinterpret_cast<char*>( &TYPE_OF_FORMAT ), 2); // PCM = 1, other values indicate some form of compression
    ofs.write(reinterpret_cast<char*>( &nChannels ), 2);
    ofs.write(reinterpret_cast<char*>( &sampleRate ), 4);
    ofs.write(reinterpret_cast<char*>( &BYTE_RATE ), 4);
    ofs.write(reinterpret_cast<char*>( &DATA_BLOCK_SIZE ), 2);
    ofs.write(reinterpret_cast<char*>( &BITS_PER_SAMPLE ), 2);
    long long int data_chunk_pos = ofs.tellp();
    ofs.write("data----", 8);   // data chunk size to be filled later

    // Write PCM data
    for (auto sample : recordBuffer){
        ofs.write(reinterpret_cast<char*>(&sample), sizeof(sample));
    }

    // Write PCM data
    //for (std::vector<int8_t>::size_type i = 0; i < recordBuffer.size(); i++)
    //    ofs.write(reinterpret_cast<char*>(&recordBuffer[i]), sizeof( recordBuffer[i]) );

    // Fix the data chunk size
    long long int file_length = ofs.tellp();
    ofs.seekp( data_chunk_pos + 4 ); // Move to DATA part of header
    long long int data_chunk_size = file_length - data_chunk_pos + CHAR_BIT;
    ofs.write(reinterpret_cast<char*>( &data_chunk_size), 4);

    // Fix the file header to contain the proper RIFF chunk size, which is (file size - 8) bytes
    long long int proper_riff_chunk_size = file_length - CHAR_BIT;
    ofs.seekp( 0 + 4 );
    ofs.write(reinterpret_cast<char*>( &proper_riff_chunk_size), 4);

    std::cout << "Saved to file " << fileName << "." << std::endl;
    return EXIT_SUCCESS;
}

RTPError::Type  RtAudioErr2RTPErr(RtAudioError &e)
{
    RTPError::Type type = RTPError::WARNING;
    switch (e.getType()){
        case RtAudioError::WARNING:
            type = RTPError::WARNING;
            break;
        case RtAudioError::DEBUG_WARNING:
            type = RTPError::DEBUG_WARNING;
            break;
        case RtAudioError::UNSPECIFIED:
            type = RTPError::UNSPECIFIED;
            break;
        case RtAudioError::NO_DEVICES_FOUND:
            type = RTPError::NO_DEVICES_FOUND;
            break;
        case RtAudioError::INVALID_DEVICE:
            type = RTPError::INVALID_DEVICE;
            break;
        case RtAudioError::MEMORY_ERROR:
            type = RTPError::MEMORY_ERROR;
            break;
        case RtAudioError::INVALID_PARAMETER:
            type = RTPError::INVALID_PARAMETER;
            break;
        case RtAudioError::INVALID_USE:
            type = RTPError::INVALID_USE;
            break;
        case RtAudioError::DRIVER_ERROR:
            type = RTPError::DRIVER_ERROR;
            break;
        case RtAudioError::SYSTEM_ERROR:
            type = RTPError::SYSTEM_ERROR;
            break;
        case RtAudioError::THREAD_ERROR:
            type = RTPError::THREAD_ERROR;
            break;
    }
    return type;
}

RTPAudio::Api RtAudioApi2RTPAudioApi(RtAudio::Api api)
{
    RTPAudio::Api res = RTPAudio::UNSPECIFIED;

    switch(api){
        case RtAudio::UNSPECIFIED:
            res = RTPAudio::UNSPECIFIED;
            break;
        case RtAudio::LINUX_ALSA:
            res = RTPAudio::LINUX_ALSA;
            break;
        case RtAudio::LINUX_PULSE:
            res = RTPAudio::LINUX_PULSE;
            break;
        case RtAudio::LINUX_OSS:
            res = RTPAudio::LINUX_OSS;
            break;
        case RtAudio::UNIX_JACK:
            res = RTPAudio::UNIX_JACK;
            break;
        case RtAudio::MACOSX_CORE:
            res = RTPAudio::MACOSX_CORE;
            break;
        case RtAudio::WINDOWS_WASAPI:
            res = RTPAudio::WINDOWS_WASAPI;
            break;
        case RtAudio::WINDOWS_ASIO:
            res = RTPAudio::WINDOWS_ASIO;
            break;
        case RtAudio::WINDOWS_DS:
            res = RTPAudio::WINDOWS_DS;
            break;
        case RtAudio::RTAUDIO_DUMMY:
            res = RTPAudio::RTAUDIO_DUMMY;
            break;
        case RtAudio::NUM_APIS:
            res = RTPAudio::NUM_APIS;
            break;
    }
    return res;
}

RtAudio::Api RTPAudioApi2RtAudioApi(RTPAudio::Api api)
{
    RtAudio::Api res = RtAudio::UNSPECIFIED;

    switch(api){
        case RTPAudio::UNSPECIFIED:
            res = RtAudio::UNSPECIFIED;
            break;
        case RTPAudio::LINUX_ALSA:
            res = RtAudio::LINUX_ALSA;
            break;
        case RTPAudio::LINUX_PULSE:
            res = RtAudio::LINUX_PULSE;
            break;
        case RTPAudio::LINUX_OSS:
            res = RtAudio::LINUX_OSS;
            break;
        case RTPAudio::UNIX_JACK:
            res = RtAudio::UNIX_JACK;
            break;
        case RTPAudio::MACOSX_CORE:
            res = RtAudio::MACOSX_CORE;
            break;
        case RTPAudio::WINDOWS_WASAPI:
            res = RtAudio::WINDOWS_WASAPI;
            break;
        case RTPAudio::WINDOWS_ASIO:
            res = RtAudio::WINDOWS_ASIO;
            break;
        case RTPAudio::WINDOWS_DS:
            res = RtAudio::WINDOWS_DS;
            break;
        case RTPAudio::RTAUDIO_DUMMY:
            res = RtAudio::RTAUDIO_DUMMY;
            break;
        case RTPAudio::NUM_APIS:
            res = RtAudio::NUM_APIS;
            break;
    }
    return res;
}


void printOpusError(int error)
{
    fprintf(stderr, "Opus: ");
    switch (error) {
        case OPUS_OK:
            fprintf(stderr, "No error.\n");
            break;
        case OPUS_ALLOC_FAIL:
            fprintf(stderr, "Memory allocation has failed.\n");
            break;
        case OPUS_BAD_ARG:
            fprintf(stderr, "One or more invalid/out of range arguments.\n");
            break;
        case OPUS_BUFFER_TOO_SMALL:
            fprintf(stderr, "Not enough bytes allocated in buffer.\n");
            break;
        case OPUS_INTERNAL_ERROR:
            fprintf(stderr, "An internal error was detected.\n");
            break;
        case OPUS_INVALID_PACKET:
            fprintf(stderr, "The compressed data passed is corrupted.\n");
            break;
        case OPUS_INVALID_STATE:
            fprintf(stderr, "An encoder or decoder structure is invalid or already freed.\n");
            break;
        default:
            fprintf(stderr, "No such error.\n");
            break;
    }
}
