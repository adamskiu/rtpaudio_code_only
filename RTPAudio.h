#ifndef RTPAUDIO_H
#define RTPAUDIO_H

#include "jrtplib3/rtpsession.h"
#include "jrtplib3/rtpsessionparams.h"
#include "jrtplib3/rtpudpv4transmitter.h"
#include "jrtplib3/rtpipv4address.h"
#include "jrtplib3/rtptimeutilities.h"
#include "jrtplib3/rtppacket.h"
#include "jrtplib3/rtpsourcedata.h"

#include "boundedBuffer.h"

#include <string>
#include <cstdint>
#include <stdexcept>
#include <iostream>
#include <fstream>

#include "opus/opus.h"

class RtAudio;

namespace RTPAudio
{
    // Will determine size of sample
    typedef unsigned long RTPAudioFormat;
    static const RTPAudioFormat RTPAUDIO_SINT8   = 0x1;   // 8-bit signed integer.
    static const RTPAudioFormat RTPAUDIO_SINT16  = 0x2;   // 16-bit signed integer.
    static const RTPAudioFormat RTPAUDIO_SINT32  = 0x8;   // 32-bit signed integer.
    static const RTPAudioFormat RTPAUDIO_FLOAT32 = 0x10;  // Normalized between plus/minus 1.0.
    static const RTPAudioFormat RTPAUDIO_FLOAT64 = 0x20;  // Normalized between plus/minus 1.0.

    static const double MSEC_IN_SEC = 1000.0;


    class RTPError : public std::runtime_error
    {
    public:
        //! Defined RTPError types.
        enum Type {
            // Audio errors
            WARNING,           /*!< A non-critical error. */
            DEBUG_WARNING,     /*!< A non-critical error which might be useful for debugging. */
            UNSPECIFIED,       /*!< The default, unspecified error type. */
            NO_DEVICES_FOUND,  /*!< No devices found on system. */
            INVALID_DEVICE,    /*!< An invalid device ID was specified. */
            MEMORY_ERROR,      /*!< An error occurred during memory allocation. */
            INVALID_PARAMETER, /*!< An invalid parameter was specified to a function. */
            INVALID_USE,       /*!< The function was called incorrectly. */
            DRIVER_ERROR,      /*!< A system driver error occurred. */
            SYSTEM_ERROR,      /*!< A system error occurred. */
            THREAD_ERROR,      /*!< A thread error occurred. */
            FORMAT_ERROR,      /*!< Handling of sample type not yer implemented. */
            // JRTPLIB errors
            NETWORK_ERROR,     /*!< JRTPLIB returns ints so I have to create my own type. */
            IP_ERROR           /*!< Bad IP specified */
        };

        //! The constructor.
        explicit RTPError( const std::string& message,
                  Type type = RTPError::UNSPECIFIED )
                : std::runtime_error(message), type_(type) {}

        //! Prints thrown error message to stderr.
        virtual void printMessage() const
        { std::cerr << '\n' << what() << "\n\n"; }

        //! Returns the thrown error message type.
        virtual const Type& getType() const { return type_; }

        //! Returns the thrown error message string.
        virtual const std::string getMessage() const
        { return std::string(what()); }

    protected:
        Type type_;
    };

    enum Api{
        UNSPECIFIED,    /*!< Search for a working compiled API. */
        LINUX_ALSA,     /*!< The Advanced Linux Sound Architecture API. */
        LINUX_PULSE,    /*!< The Linux PulseAudio API. */
        LINUX_OSS,      /*!< The Linux Open Sound System API. */
        UNIX_JACK,      /*!< The Jack Low-Latency Audio Server API. */
        MACOSX_CORE,    /*!< Macintosh OS-X Core Audio API. */
        WINDOWS_WASAPI, /*!< The Microsoft WASAPI API. */
        WINDOWS_ASIO,   /*!< The Steinberg Audio Stream I/O API. */
        WINDOWS_DS,     /*!< The Microsoft DirectSound API. */
        RTAUDIO_DUMMY,  /*!< A compilable but non-functional API. */
        NUM_APIS        /*!< Number of values in this enum. */
    };

    // Supports only opus_int16 samples, hence no sampleSize passed to constructors or open function
    class WavFile {
    public:
        WavFile()
                : m_sampleRate(0),
                  m_channels(0),
                  m_filename("test.wav") {}

        WavFile(const std::string &fileName,
                unsigned int       sampleRate,
                unsigned int       nChannels)
                : m_sampleRate(sampleRate),
                  m_channels(nChannels),
                  m_filename(fileName)
        {
            openAndWriteHeader();
        }

        ~WavFile()
        {
            // This can be done before closing the file if execution speed is priority
            // But this way we can open the file even if program crashes
            long long int file_length = m_file.tellp(); // need to come back to this place after correction
            m_file.seekp( DATA_CHUNK_POS ); // Move to DATA part of header
            // Fix the data chunk size
            long long int data_chunk_size = file_length - HEADER_LENGTH;
            m_file.write(reinterpret_cast<char*>( &data_chunk_size), 4);

            // Fix the file header to contain the proper RIFF chunk size, which is (file size - 8) bytes
            long long int proper_riff_chunk_size = file_length - CHAR_BIT;
            m_file.seekp( RIFF_CHUNK_SIZE_POS );
            m_file.write(reinterpret_cast<char*>( &proper_riff_chunk_size), 4);
            m_file.seekp(file_length);
            if (m_file.is_open()) {
                m_file.close();
            }
            printf("Saved to %s.\n", m_filename.c_str());
        }

        void open(const std::string &fileName,
                  unsigned int sampleRate,
                  unsigned int channels)
        {
            if (m_file.is_open()) {
                fprintf(stderr, "File already opened! Doing nothing...\n");
                return;
            }

            m_filename = fileName;
            m_sampleRate = sampleRate;
            m_channels = channels;
            openAndWriteHeader();
        }

        // TODO : should check if file is open (default constructor doesn't open the file
        void writeSamples(const opus_int16 *begin, const opus_int16 *end)
        {
            m_file.write(reinterpret_cast<const char*>(begin), (end - begin) * sizeof(opus_int16));
        }

    private:
        const long long int HEADER_LENGTH = 44;
        const long long int DATA_CHUNK_POS = 40;
        const long long int RIFF_CHUNK_SIZE_POS = 4;

        std::ofstream  m_file;
        unsigned int   m_sampleRate;
        unsigned int   m_channels;
        std::string    m_filename;

        void openAndWriteHeader()
        {
            m_file.open(m_filename, std::ios::binary);
            if (!m_file){
                fprintf(stderr, "Error opening file %s.\n", m_filename.c_str());
                fprintf(stderr, "Opening with filename test.wav instead.\n");
                m_file.open("test.wav", std::ios::binary);
                m_filename = "test.wav";
            }

            int BITS_PER_SAMPLE = sizeof(opus_int16) * CHAR_BIT;
            int LENGTH_OF_FORMAT_DATA = 16;
            int TYPE_OF_FORMAT = 1;   // PCM
            int BYTE_RATE = (m_sampleRate * BITS_PER_SAMPLE * m_channels) / CHAR_BIT;
            int DATA_BLOCK_SIZE = (BITS_PER_SAMPLE * m_channels) / CHAR_BIT; // size of two samples, one for each channel, in bytes

            // Write header - first 44 bytes.
            m_file.write("RIFF----WAVEfmt ", 16);  // file size to be filled later at position RIFF_CHUNK_SIZE_POS
            m_file.write(reinterpret_cast<char*>( &LENGTH_OF_FORMAT_DATA), 4); // 16 for PCM
            m_file.write(reinterpret_cast<char*>( &TYPE_OF_FORMAT ), 2); // PCM = 1, other values indicate some form of compression
            m_file.write(reinterpret_cast<char*>( &m_channels ), 2);
            m_file.write(reinterpret_cast<char*>( &m_sampleRate ), 4);
            m_file.write(reinterpret_cast<char*>( &BYTE_RATE ), 4);
            m_file.write(reinterpret_cast<char*>( &DATA_BLOCK_SIZE ), 2);
            m_file.write(reinterpret_cast<char*>( &BITS_PER_SAMPLE ), 2);
            m_file.write("data----", 8);   // data chunk size to be filled later at position DATA_CHUNK_POS
        }
    };

    struct clientInfo{
        clientInfo() :
                sampleRate(0),
                channels  (0),
                sampleSize(0),
                CNAME     ("Unknown"),
                decoder   (nullptr),
                decoded   (nullptr)
                {}

        clientInfo(unsigned int sampleRate,
                   unsigned int channels,
                   unsigned int sampleSize,
                   int          frameSize,
                   uint32_t     SSRC)
                   : sampleRate(sampleRate),
                     channels(channels),
                     sampleSize(sampleSize),
                     CNAME("Unknown")
        {
            int error = OPUS_OK;
            decoder = opus_decoder_create(sampleRate,
                                          channels,
                                          &error);
            if (OPUS_OK != error) {
                fprintf(stderr, "Opus: %s\n", opus_strerror(error));
            }
            // TODO: what is frames size would change?
            decoded = new opus_int16[channels * frameSize];

            std::stringstream stream;
            stream << std::hex << SSRC;
            std::string filename(stream.str());
            for (auto & c: filename){
                c = toupper(c);
            }

            filename = filename + ".wav";
            wavFile.open(filename, sampleRate, channels);
        }
        ~clientInfo() {
            delete[] decoded;
            opus_decoder_destroy(decoder);
        }

        unsigned int        sampleRate;
        unsigned int        channels;
        unsigned int        sampleSize;
        std::string         CNAME;
        OpusDecoder        *decoder;
        opus_int16         *decoded;
        WavFile             wavFile;
    };

    class MyRTPSession : public jrtplib::RTPSession{
    protected:

        void OnRTCPSenderReport  (jrtplib::RTPSourceData *dat)      override;
        void OnRTCPReceiverReport(jrtplib::RTPSourceData *dat)      override;
        void OnNewSource         (jrtplib::RTPSourceData *dat)      override;
        void OnBYEPacket         (jrtplib::RTPSourceData *dat)      override;
        void OnRemoveSource      (jrtplib::RTPSourceData *dat)      override;
        void OnRTCPSDESItem      (jrtplib::RTPSourceData *dat,
                                  jrtplib::RTCPSDESPacket::ItemType t,
                                  const void *itemData,
                                  size_t itemLen)          override;
        void OnPollThreadStep()                            override;

        void ProcessRTPPacket(const jrtplib::RTPSourceData &dat,
                              const jrtplib::RTPPacket &packet);

    private:
        std::map<unsigned int, std::string> m_SSRCtoCNAME;
        std::map<unsigned int, clientInfo*>  m_SSRCtoClient;
        void printClients();
    };

    struct RTPRecordParams{
        RTPAudio::Api  api;
        RTPAudioFormat sampleType;
        unsigned int   channels;
        unsigned int   sampleRate;
        unsigned int   device;
        unsigned int   nBufferFrames;
    };

    struct RTPConnectionParams{
        uint16_t    portBase;
        uint16_t    destPort;
        std::string destIP;
        std::string localName;
    };

    struct RTPBufferData{
        unsigned int            channels;
        unsigned int            sampleRate;
        unsigned long           bufferBytes;
        unsigned int            sampleSize;
        bounded_buffer<int8_t> *samples;
    };

    class RTPServer{
    public:
        void initAndListen(uint16_t portBase);
    private:
        MyRTPSession m_session;
    };

    class RTPClient{
    public:
        RTPClient();
        ~RTPClient();

        RTPBufferData*      setup                (RTPRecordParams recordParams, RTPConnectionParams connParams );
        void                SendPacket           (const void *data,size_t len);
        void                sendBYE              (const jrtplib::RTPTime &maxWaitTime, std::string byeReason);
        static void         getCompiledApis      (std::vector<RTPAudio::Api> &apis);
        static unsigned int getDefaultInputDevice(RTPAudio::Api api);
        static std::string  getApiDisplayName    (RTPAudio::Api api);
        static unsigned int getDeviceCount       (RTPAudio::Api api);
        static int          displayDeviceInfo    (RTPAudio::Api api, unsigned int deviceNum);
        void                startRecording       ();

    private:

        bool deviceValid(RtAudio &audio, unsigned int deviceNum);

        RtAudio       *m_audio;
        RTPBufferData  m_bufferData;
        MyRTPSession   m_session;
        OpusEncoder   *m_encoder;
        unsigned char *m_encoded;
        unsigned long long m_encodingTimesSum;
        unsigned long long m_encodedPacketSizeSum;
        unsigned long long m_noOfEncodings;
    };

} // end namespace

int saveToWAV(const std::string   &fileName,
              std::vector<opus_int16 > &recordBuffer,
              unsigned int         sampleSize,
              unsigned int         sampleRate,
              unsigned int         nChannels);

#endif //RTPAUDIO_H
