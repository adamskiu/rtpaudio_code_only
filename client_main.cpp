#include "jrtplib3/rtptimeutilities.h"
#include <iostream>
#include <string>
#include "RTPAudio.h"
#include "boundedBuffer.h"

bool sendingDone = false;

// Platform-dependent signal handlers
#if defined(WIN32)
#include <windows.h>
BOOL WINAPI CtrlCHandler(DWORD fdwCtrlType)
{
    switch (fdwCtrlType)
    {
        // Handle the CTRL-C signal.
        case CTRL_C_EVENT:
            printf("Ctrl-C event\n\n");
            sendingDone = true;
            return TRUE;
        default:
            return FALSE;
    }
}
#endif
#if defined(__linux__) // Unix
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
void ctrlCHandler(int sig){
    printf("Caught signal \n");
    sendingDone = true;
}
#endif

using namespace RTPAudio;

int main(int argc, char **argv) {

#ifdef RTP_SOCKETTYPE_WINSOCK
    WSADATA dat;
    WSAStartup(MAKEWORD(2, 2), &dat);
#endif

#if defined(WIN32)
    SetConsoleCtrlHandler(CtrlCHandler, TRUE);
#endif
#if defined(__linux__)
    struct sigaction sigIntHandler;

   sigIntHandler.sa_handler = ctrlCHandler;
   sigemptyset(&sigIntHandler.sa_mask);
   sigIntHandler.sa_flags = 0;

   sigaction(SIGINT, &sigIntHandler, NULL);
#endif


   if (argc < 4) {
       printf( "Usage: %s IP sample_rate miliseconds_sent internal_buffer_size(optional)\n"
              "IP - IP of the server\n"
              "sample_rate - sample rate of recorded sound\n"
              "Possible values:\n"
              "8000 16000 24000 48000\n"
              "miliseconds_sent - How many miliseconds are sent in a packet.\n"
              "Possible values:\n"
              "2.5 5 10 20 40 60 80 100 120\n"
              "internal_buffer_size - size of callback buffer(default is 512 frames)\n",
              argv[0]);
       return 0;
   }

   std::string IP = std::string(argv[1]);
   unsigned int sampleRate = std::atoi(argv[2]);
   double miliseconds = std::atof(argv[3]);
   unsigned int nBufferFrames = 512;
   if (argc > 4) {
       nBufferFrames = std::atoi(argv[4]);
   }

    std::vector<RTPAudio::Api> apis;
    RTPClient::getCompiledApis(apis);

    struct RTPRecordParams recordParams;
    recordParams.sampleRate = sampleRate;
    recordParams.nBufferFrames = nBufferFrames;
    recordParams.channels = 1;
    recordParams.sampleType = RTPAUDIO_SINT16;
    recordParams.api = apis[0];
    recordParams.device = RTPClient::getDefaultInputDevice(recordParams.api);
    //recordParams.device = 2; // Laptop mic

    RTPConnectionParams connectionParams;
    connectionParams.destIP = IP;
    connectionParams.portBase = 5004;
    connectionParams.destPort = 5000;
    connectionParams.localName = "Adam-Client";

    RTPClient client;
    RTPBufferData *buffData = client.setup(recordParams, connectionParams);
    if (buffData == nullptr) {
        return EXIT_FAILURE;
    }

    /*
     * Packet len should be 2.5ms, 5ms, 10ms, 20ms, 40ms, 60ms, 80ms, 100ms or 120ms of sound
     * For example for sound of 48000Hz, 2 channels, of which sample size is 2 bytes
     * Buffer of 100ms has 480 frames of sound
     * 48000 * 0.100 = 480
     * Frame is sample for each channel
     */

    size_t frameSize = buffData->channels * buffData->sampleSize;
    // Specify how much do you want to send in one packet
    size_t framesInPacket = buffData->sampleRate * (miliseconds / RTPAudio::MSEC_IN_SEC);
    size_t bytesInPacket = frameSize * framesInPacket;

    printf("Sample rate: %dHz\n", buffData->sampleRate);
    printf("Sample size: %d byte(s)\n", buffData->sampleSize);
    printf("Channels: %d\n", buffData->channels);
    printf("Bytes in packet: %zu\n", bytesInPacket);

    auto *packet = new int8_t[bytesInPacket];
    // Start recording
    try {
        client.startRecording();
    }
    catch (RTPError &e) {
        fprintf(stderr, "%s\n", e.getMessage().c_str());
        return EXIT_FAILURE;
    }

    printf("Recording started...\n");
    printf("Sending...\n");

    jrtplib::RTPTime startTime = jrtplib::RTPTime::CurrentTime();

    jrtplib::RTPTime fillBuffStart = jrtplib::RTPTime::CurrentTime();
    jrtplib::RTPTime fillBuffStop = fillBuffStart;
    unsigned long long noOfSentPackets = 0;
    unsigned long long sumOfFillTimes = 0;

    while (!sendingDone) {
        // Fill the packet
        fillBuffStart = jrtplib::RTPTime::CurrentTime();
        for (unsigned int i = 0; i < bytesInPacket; i++) {
            buffData->samples->pop_back(&packet[i]);
        }
        fillBuffStop = jrtplib::RTPTime::CurrentTime();
        client.SendPacket(packet, bytesInPacket);
        noOfSentPackets++;

        fillBuffStop -= fillBuffStart;
        sumOfFillTimes += fillBuffStop.GetMicroSeconds();
    }

    jrtplib::RTPTime endTime = jrtplib::RTPTime::CurrentTime();
    endTime -= startTime;
    printf("Sent.\n");
    printf("Transmission took %.3f seconds.\n", endTime.GetDouble());
    printf("Filling packet time average: %llu microseconds.\n",
           sumOfFillTimes / noOfSentPackets);

    std::string BYEReason = "End of stream";
    client.sendBYE(jrtplib::RTPTime(10, 0), BYEReason);

    delete[] packet;

#ifdef RTP_SOCKETTYPE_WINSOCK
    WSACleanup();
#endif
    return 0;
}