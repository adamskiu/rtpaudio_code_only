#include <iostream>
#include "jrtplib3/rtptimeutilities.h"
#include "RtAudio.h"
#include "boundedBuffer.h"
#include "RTPAudio.h"

bool done = false;

// Windows signal handler
#if defined(WIN32)
#include <windows.h>
BOOL WINAPI CtrlCHandler(DWORD fdwCtrlType)
{
    switch (fdwCtrlType)
    {
        // Handle the CTRL-C signal.
        case CTRL_C_EVENT:
            printf("Ctrl-C event\n\n");
            done = true;
            return TRUE;
        default:
            return FALSE;
    }
}
#endif

// Linux signal handler
#if defined(__linux__)
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
void ctrlCHandler(int s){
    printf("\nCaught Ctrl-C\n");;
    done = true;
}
#endif


// ########################## MAIN STARTS HERE ############################
int main()
// ########################################################################
{
#ifdef RTP_SOCKETTYPE_WINSOCK
    WSADATA dat;
    WSAStartup(MAKEWORD(2,2),&dat);
#endif

    // Ctrl-C handling
#if defined(WIN32)
    SetConsoleCtrlHandler(CtrlCHandler, TRUE);
#endif

#if defined(__linux__)
    struct sigaction sigIntHandler;

   sigIntHandler.sa_handler = ctrlCHandler;
   sigemptyset(&sigIntHandler.sa_mask);
   sigIntHandler.sa_flags = 0;

   sigaction(SIGINT, &sigIntHandler, NULL);
#endif
   using namespace RTPAudio;

   RTPServer server;
   uint16_t port = 5000;

   try{
       server.initAndListen(port);
   }
   catch(RTPError &e){
       std::cerr << e.getMessage();
       return EXIT_FAILURE;
   }


   printf("Waiting for some data...\n");

    jrtplib::RTPTime startTime = jrtplib::RTPTime::CurrentTime();

    // Waiting for packets...
   while(!done) {

   }

    jrtplib::RTPTime endTime = jrtplib::RTPTime::CurrentTime();
    endTime -= startTime;

    printf("Server was up for %.2f seconds.\n", endTime.GetDouble());


#ifdef RTP_SOCKETTYPE_WINSOCK
    WSACleanup();
#endif
    return 0;
}